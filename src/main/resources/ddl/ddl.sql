-- DROP SCHEMA auction;

CREATE SCHEMA auction AUTHORIZATION postgres;

-- Drop table

-- DROP TABLE auction.auction;

CREATE TABLE auction.auction (
	auction_id int8 NOT NULL,
	auction_proceeds float4 NULL,
	CONSTRAINT auction_pk PRIMARY KEY (auction_id)
);

-- Drop table

-- DROP TABLE auction.bid;

CREATE TABLE auction.bid (
	bid_id bigint NULL,
	bid_bidder varchar NOT NULL,
	bid_value float4 NULL,
	auction_id bigint NULL,
	CONSTRAINT bid_pk PRIMARY KEY (bid_id),
	CONSTRAINT bid_fk FOREIGN KEY (auction_id) REFERENCES auction.auction(auction_id)
);

-- auction.auction_seq definition

-- DROP SEQUENCE auction.auction_seq;

CREATE SEQUENCE auction.auction_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
-- auction.bid_seq definition

-- DROP SEQUENCE auction.bid_seq;

CREATE SEQUENCE auction.bid_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;