package com.auction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.auction.dto.AuctionResponseDTO;
import com.auction.entity.Bid;
import com.auction.service.AuctionService;

@RestController
@RequestMapping(value = "/auction")
public class AuctionRestController {
	
	@Autowired
	AuctionService auctionService;
	
	@PostMapping(value = "/")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Long> createAuction(){
		return ResponseEntity.status(201).body(auctionService.processAuctionInstuction());
	}
	
	@PostMapping(value = "/bid")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Bid> initBidInstruction(@RequestBody Bid bidInfo){
		Bid response = auctionService.processBidInstuction(bidInfo);
		return ResponseEntity.status(201).body(response);
	}
	
	@GetMapping(value = "/result/{auctionId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<AuctionResponseDTO> retriveAuctionClosingResult(@RequestParam  Long auctionIdentifier){
		AuctionResponseDTO response = auctionService.getAuctionResult(auctionIdentifier);
		return ResponseEntity.status(201).body(response);
	}


}
