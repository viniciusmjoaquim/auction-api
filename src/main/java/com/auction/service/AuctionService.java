package com.auction.service;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auction.dto.AuctionResponseDTO;
import com.auction.entity.Auction;
import com.auction.entity.Bid;
import com.auction.repository.AuctionRepository;
import com.auction.repository.BidRepository;

@Component
public class AuctionService {

	@Autowired
	AuctionRepository auctionRepository;

	@Autowired
	private BidRepository bidRepository;

	public Long processAuctionInstuction() {

		Auction auction = auctionRepository.save(new Auction());
		return auction.getId();
	}

	public AuctionResponseDTO getAuctionResult(Long auctionId) {

		Optional<Auction> auction = auctionRepository.findById(auctionId);

		if (auction.isPresent()) {
			// Menor lance único vence
			Set<Bid> uniqueList = auction.get().getBids().stream()
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())) 
					.entrySet().stream() 
					.filter(m -> m.getValue() < 2)
					.map(Map.Entry::getKey).collect(Collectors.toSet());
			
			Optional<Bid> winnerBid = uniqueList .stream()
				      .min(Comparator.comparing(Bid::getValue));

			// Output: o jogador vencedor (nome), valor do lance e a arrecadação total
			return new AuctionResponseDTO(winnerBid.get().getBidder(), winnerBid.get().getValue(),
					auction.get().getProceeds());
		}

		return null;
	}

	public Bid processBidInstuction(Bid bid) {
		// Lances com duas casas decimais, positivo > 0

		// Pode receber até 999 lances, ignorar lances após isso
		int totalBids = bidRepository.getAuctionTotalBids(bid.getAuction().getId());
		if (totalBids == 999) {
			return bid;
		}
		// Arrecadação: total de lances * 0.98 (ou seja, o custo do lance é 0.98)
		Optional<Auction> auctionOptional = auctionRepository.findById(bid.getAuction().getId());
		if (auctionOptional.isPresent()) {
			Auction auctionEntity = auctionOptional.get();
			auctionEntity.setProceeds(auctionEntity.getProceeds() + 0.98);
			auctionRepository.save(auctionEntity);
		} else {

		}
		return bidRepository.save(bid);
	}
}
