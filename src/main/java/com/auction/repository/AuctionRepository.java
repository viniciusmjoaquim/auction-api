package com.auction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.auction.entity.Auction;

@Component
public interface AuctionRepository extends JpaRepository<Auction, Long>{
}
