package com.auction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.auction.entity.Bid;

@Component
public interface BidRepository extends JpaRepository<Bid, Long>{
	
	@Query(value = "select count(*) from Bid b where b.auction.id = ?1")
	int getAuctionTotalBids(Long AuctionId);
}
