package com.auction.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "auction")
@Data
public class Auction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auction_seq")
	@SequenceGenerator(name = "auction_seq", sequenceName = "auction_seq", allocationSize = 1)
	@Column(name = "auction_id", unique = true, updatable = false, nullable = false)
	private Long id;
	
	@JsonIgnore
	@Column(name = "auction_proceeds")
	private double proceeds;
	
	@OneToMany(mappedBy = "auction")
	@JsonIgnore
	private List<Bid> bids;

}
