package com.auction.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "bid")
@Data
public class Bid implements Comparable<Bid>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bid_seq")
	@SequenceGenerator(name = "bid_seq", sequenceName = "bid_seq", allocationSize = 1)
	@Column(name = "bid_id", unique = true, updatable = false, nullable = false)
	@JsonIgnore
	private Long id;
	@Column(name = "bid_bidder")
	private String bidder;
	
	@Column(name = "bid_value")
	private double value;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "auction_id")
	private Auction auction;

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bid other = (Bid) obj;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		return true;
	}


	@Override
	public int compareTo(Bid other) {
		return Double.compare(getValue(), other.getValue());
	}
}
