package com.auction.dto;

import lombok.Data;

@Data
public class AuctionResponseDTO {
	
	public AuctionResponseDTO(String winner, double bidValue, double totalProceeds) {
		super();
		this.winner = winner;
		this.bidValue = bidValue;
		this.totalProceeds = totalProceeds;
	}
	
	private String winner;
	private double bidValue;
	private double totalProceeds;

}
