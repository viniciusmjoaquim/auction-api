package com.auction.test;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.auction.controller.AuctionRestController;
import com.auction.service.AuctionService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AuctionRestController.class)
public class AuctionApplicationTests {
	
	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
	private AuctionService auctionService; 

	@Test
	public void case1() throws Exception {
		mockMvc.perform(post("/auction/")
		        .contentType("application/json"))
		        .andExpect(status().isCreated());
	}
}
